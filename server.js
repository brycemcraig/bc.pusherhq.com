/* ----------------------------------------------------------------------------
    Copyright (c) 2012 Pusher, Inc.

    The content of this file is the property of Pusher Inc. and is protected
    by copyright law. It is not permissable to use the content herein in any
    manner without express prior written permission from Pusher Inc.
   ----------------------------------------------------------------------------
*/

//===========================================================================//
// Require files and globals
//===========================================================================//

var express     = require('express'),
    path        = require('path'),
    fs          = require('fs'),
    os          = require('os'),
    _           = require('underscore');

var pusher      = 
{
    platform    : require('pusher.platform'),
};
        
//===========================================================================//
// Configuration
//===========================================================================//    

var app = express();

var platformConfig = 
{

    logToFile   : false,
    debug       : true,

    app         : app,
    
    templatesPartials : "templates/partials",
    templateDefaults :
    {
        site :
        {
            title : "Basecamp API Test"
        },
        page : 
        {
            title : " Basecamp API Test"
        },
        user :
        {
        }
    },
    templateFunctions :
    {
        '*' : function (req, data, done)
        {
            done();
        }
    },
};


pusher.platform.config(platformConfig);

//
// The order of the middleware stack is *fragile*.  Be careful with any changes.
//
app.use(express.cookieParser("t/31gv5hrD9/gGM9e67Yz3/OmQw=P4hA3NZAN/O/mxo/jPj2"));
app.use(express.session());
app.use(express.bodyParser());
app.use(express.methodOverride());

app.use(pusher.platform.modelRoutes());
app.use(app.router);

app.use(pusher.platform.renderPageMiddleware());
app.use(express.static(__dirname + '/public'));     // Regular file serving of all files under "public"
app.use(function(req, res, next){
    res.set('Content-Type', 'text/plain');
    res.send('404', "404: Page Not Found");
});


//===========================================================================//
// Routes
//===========================================================================//    
  
   
app.get('/', function(req, res, next) 
{
    pusher.platform.renderPage(req, res, next, "/index");
});


//===========================================================================//
// Get Basecamp Projects
//===========================================================================//  

    var exec = require('child_process').exec,
    child;

    child = exec("curl -u brycemcraig:pine314!basecamp -H 'User-Agent: MyApp (info@pusherhq.com)' https://basecamp.com/1765792/api/v1/projects.json",
        function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
            if (error !== null) {
                console.log('exec error: ' + error);
        }
    });
    


//===========================================================================//
// Start the server
//===========================================================================// 

pusher.platform.log("Starting server on port 8708");
pusher.platform.log("Hostname: " + require("os").hostname());
console.log("Starting server on port 8708");
app.listen(8708);

